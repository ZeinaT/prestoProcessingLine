#!/usr/bin/perl -w

use strict; 
use warnings;

use Getopt::Long;

# Tout passer en UTF-8
use v5.12;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Désactiver les warnings sur les fonctions "expérimentales" (qui marchent très bien).
no if ($] >= 5.018), 'warnings' => 'experimental';

############################
# Traitement des arguments #
############################

# Lecture des arguments
my $config_parts = 0;
my $config_keep = 0;
my $config_boundary = "";
GetOptions(
  'part|p=s'=>\$config_parts,
  'keep|k=s'=>\$config_keep,
  'boundary|b=s'=>\$config_boundary,
);

my $boundTag = '';
my $boundVal = '';
if($config_boundary=~m/(.*)="(.*?)"/) {
  $boundTag = $1;
  $boundVal = $2;
}
if(!$boundTag || !$boundVal) {
  die("L'argument -b doit avoir un paramètre de la forme tag=\"valeur\"; par exemple pos=\"Fs\"\n");
}

my @keep = split(/\s*,\s*/, $config_keep);

#######################
# Traitement du texte #
#######################

my $line = <STDIN>;
chomp $line;
print "$line\n";
my @heads = split(/\t/, $line);

my %lineHash = ();
my $part = 0;
while($line = <STDIN>) {
  chomp $line;

  # Décomposer la ligne
  my $i=0;
  foreach my $cel (split(/\t/, $line)) {
    $lineHash{$heads[$i] || ''} = $cel;  # Attribuer des valeurs aux "tags" qui constituent la ligne
    ++$i;
  }
  
  if($part ~~ @keep) {  # Ligne dans une partie à garder
    print "$line\n";
  }
  
  if($lineHash{$boundTag} eq $boundVal) {  # Frontière de phrase
    ++$part;
  }
  
  if($part == $config_parts) {
    $part = 0;
  }
  
}
