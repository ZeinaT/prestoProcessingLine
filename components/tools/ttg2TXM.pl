#!/usr/bin/perl -w

use strict;
use warnings;

use HTML::Entities;
use Getopt::Long;

# Tout passer en UTF-8
use v5.12;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";
use open qw(:std :utf8);
use Encode qw(decode_utf8);
use File::Slurp;

print "<text>\n";
while (my $line = <STDIN>) {
  chomp($line);

	$line=encode_entities($line, '&<"');

  if($line=~m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {    # TODO Récupérer la liste des tags directement dans le header du texte
    my ($form, $morph, $lemma, $sem) = split(/\s*\t\s*/, $line);
    (my $shortMorph = $morph) =~ s/^(.(\+.)?).*/$1/;
    print "<w pos=\"$morph\" spos=\"$shortMorph\" lemma=\"$lemma\" sem=\"$sem\">$form</w>\n";
  }
  elsif($line=~m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {
    my ($form, $morph, $lemma) = split(/\s*\t\s*/, $line);
    (my $shortMorph = $morph) =~ s/^(.(\+.)?).*/$1/;
    print "<w pos=\"$morph\" spos=\"$shortMorph\" lemma=\"$lemma\">$form</w>\n";
  }
  elsif($line=~m/^([^\t]+?)\t([^\t]+?)$/) {
    my ($form, $morph) = split(/\s*\t\s*/, $line);
    (my $shortMorph = $morph) =~ s/^(.(\+.)?).*/$1/;
    print "<w pos=\"$morph\" spos=\"$shortMorph\" lemma=\"\">$form</w>\n";
  }
  elsif($line=~m/^([^\t]+?)$/) {
    print "\n";
  }
}
print "</text>\n";
