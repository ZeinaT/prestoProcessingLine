#!/usr/bin/perl -w

use strict; 
use warnings;

# Tout passer en UTF-8
use v5.12;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Arguments
my $corpusName = $ARGV[0];
my $path = $ARGV[1];
my $eosCat = $ARGV[2];

# Un fichier par table CSV à peupler
open(my $fCollection, '>', "$path/collection.csv");
open(my $fLang, '>', "$path/lang.csv");
open(my $fCorpus, '>', "$path/corpus.csv");
open(my $fCorpusProperties, '>', "$path/corpus_properties.csv");
open(my $fTokens, '>', "$path/tokens.csv");
open(my $fForms, '>', "$path/form.csv");
open(my $fLemmas, '>', "$path/lemma.csv");
open(my $fCats, '>', "$path/cat.csv");
open(my $fFeas, '>', "$path/fea.csv");
open(my $fSentences, '>', "$path/sentence.csv");
open(my $fStructs, '>', "$path/struct.csv");
open(my $fRelations, '>', "$path/relation.csv");
open(my $fDependencies, '>', "$path/dependencies.csv");

# Compteurs et indexes pour la génération des CSV
my $idText = 0;
my $idCollection = 1;
my $idToken = 0;
my $idSent = 1;
my $idParagraph = 1;
my $idStruct = 0;
my %cats = ();
my %lemmas = ();
my %forms = ();
my %feas = ();
my %relations = ();
my %tokenInSent = ();
my $nbCats = 1;
my $nbForms = 1;
my $nbLemmas = 1;
my $nbFeas = 1;
my $nbRelations = 1;
my $idTokenInSent = 1;
my $idRelation = 1;

# Est-on dans une partie du corpus qu'on veut enregistrer, ou bien dans les méta-données ?
my $doRecord = 0; 

# Pour la génération du config.json
my %corpusProperties = ();

while (my $line = <STDIN>) {
  chomp $line;
  
  if($line=~m/^\s*<(?:presto:)?text (.*)>/) {  # Nouveau texte
    ++$idText;
    my $argsLine = $1;
    
    my %args;
    $argsLine=~s/""/" "/g;
    my @a = split(/(?:="|")/, $argsLine);
    for(my $i=0; $i<@a; $i+=2) {
      (my $k = $a[$i]) =~ s/^ *//;
      (my $v = $a[$i+1]) =~ s/^ *//;
      print STDERR "$k=$v\n";
      if($k && $v) {
        $args{$k} = $v;
        if(exists $corpusProperties{$k}) {
          if($corpusProperties{$k}!~m/(^|\t)$v(\t|$)/) {
            $corpusProperties{$k} .= "\t$v";
          }
        }
        else {
          $corpusProperties{$k} = $v;
        }
        print $fCorpusProperties "$idText\t$k\t$v\n";
      }
    }
    
    # Valeurs par défaut
    if(! $args{'name'}) {
      $args{'name'} = $args{'ref'};
    }
    if(! $args{'fileName'}) {
      $args{'fileName'} = $args{'name'};
    }
    if(! $args{'title'}) {
      $args{'title'} = $args{'name'};
    }
    
    if(! $args{'title'}) {
      print STDERR "Manque un attribut title ou name:\n";
      print STDERR "$line\n";
      die();
    }
    
    if(! $args{'licence'}) {
      print STDERR "Manque un attribut licence:\n";
      print STDERR "$line\n";
      die();
    }
    
    print $fCorpus "$idText\t1\t$args{'name'}\t$args{'fileName'}\t1\t$args{'title'}\t\t$args{'auteur'}\t1\t$args{'title'}\t\t\t\t\t\t\t$args{'licence'}\n";
    
    if($idStruct < $idText) {
      $idStruct = $idText;
    }
    
  }
  elsif($line=~m/^\s*<(?:presto:)?body>/) {
    $doRecord = 1;
  }
  elsif($line=~m/^\s*<\/(?:presto:)?body>/) {
    $doRecord = 0;
  }
  elsif($line=~m/^\s*<(?:presto:)?div(?:>| type="(.*?)"| .*? type="(.*?)")/) {  # Nouveau div
    my $type = $1;
    if(! $type) {
      $type = 'div';
    }
    ++$idStruct;
    print $fStructs "$idStruct\t1\t$idText\t$type\n";
  }
  #elsif($line=~m/^\s*<\/(?:presto:)?div>/) {
  #  print $fStructs "$idStruct\t1\t$idText\tdiv\n";
  #}
  elsif($line=~m/^\s*<(?:presto:)?p[ >]/) {  # Nouveau paragraphe
    ++$idParagraph;
  }
  elsif($line=~m/^\s*<\/sent>/) {  # Fin de phrase, lorsqu'elle est codée par une balise
    print $fSentences "$idSent\t1\t$idText\t$idText\t$idStruct\t$idParagraph\t$idText-$idParagraph-$idSent\n";
    ++$idSent;
    #%tokenInSent = ();
    $idTokenInSent = 0;
  }
  elsif($line=~m/^\s*REL\t(.*?)\t(.*?)\t(.*?)\s*$/) {  # Format (REL, relation, gouverneur, dépendant)
    my $rel = $1;
    my $gov = $2;
    my $dep = $3;
    if(exists $tokenInSent{$gov}) {  # Pour certains analyseurs (Bonsai-MALT), la tête de l'arbre a une relation avec un token fictif "0"
      if(! exists $relations{$rel}) {
        print $fRelations "$nbRelations\t$rel\n";
        $relations{$rel} = $nbRelations++;
      }
      print $fDependencies "$idRelation\t1\t$tokenInSent{$gov}\t$tokenInSent{$dep}\t$relations{$rel}\n";
      ++$idRelation;
    }
  }
  else {
  
    # Décodage d'un token
    my $form = '';
    my $cat = '';
    my $fea = '';
    my $lemma = '';
    my $isToken = 0;
    if($line=~m/^\s*([^\t]*)\t([^\t]*)\t([^\t]*)\s*$/) {  # Format TreeTagger (forme, cat, lemme)
      ++$idToken;
      $form = $1;
      $cat = $2;
      $lemma = $3;
      $fea = $cat;
      $isToken = 1;
    }
    elsif($line=~m/^\s*TOKEN\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\s*$/) {  # Format (TOKEN, numéro, forme, cat, morpho, lemme)
      ++$idToken;
      $form = $2;
      $cat = $3;
      $fea = $4;
      $lemma = $5;
      $tokenInSent{$1} = $idToken;
      $isToken = 1;
    }    

    # Ajout du token
    if($isToken) {
    
      if($idSent < $idStruct) {
        $idSent = $idStruct;
      }
      
      if(! exists $forms{$form}) {
        $forms{$form} = $nbForms;
        print $fForms "$nbForms\t$form\n";
        ++$nbForms;
      }
      if(! exists $lemmas{$lemma}) {
        $lemmas{$lemma} = $nbLemmas;
        print $fLemmas "$nbLemmas\t$lemma\n";
        ++$nbLemmas;
      }
      if(! exists $cats{$cat}) {
        print $fCats "$nbCats\t$cat\n";
        $cats{$cat} = $nbCats++;
      }
      if(! exists $feas{$fea}) {
        print $fFeas "$nbFeas\t$fea\n";
        $feas{$fea} = $nbFeas++;
      }
      
      print $fTokens "$idToken\t1\t$idText\t$idText\t$idStruct\t$idParagraph\t$idSent\t$forms{$form}\t$lemmas{$lemma}\t$cats{$cat}\t$feas{$fea}\t$idTokenInSent\t0\t1\n";
      
      ++$idTokenInSent;
      
      if($cat eq $eosCat) {  # Fin de phrase, lorsqu'elle est codée par une catégorie
        print $fSentences "$idSent\t1\t$idText\t$idText\t$idStruct\t$idParagraph\t$idText-$idParagraph-$idSent\n";
        ++$idSent;
        #%tokenInSent = ();
        $idTokenInSent = 0;
      }
    }
  }
  
  
}

print $fCollection "1\t$corpusName\t1\t$idToken\t$idToken\t$idSent\n";
print $fLang "1\tunk\n";

close($fCorpus);
close($fCorpusProperties);
close($fCollection);
close($fTokens);
close($fForms);
close($fLemmas);
close($fCats);
close($fFeas);
close($fLang);
close($fSentences);
close($fStructs);
close($fRelations);
close($fDependencies);

open(my $fConfig, '>', "$path/config.json");

print $fConfig "{\n";
print $fConfig '"collection": "TODO - Nom de la collection (pour regrouper des corpus)",'."\n";
print $fConfig '"url": "TODO - URL du projet",'."\n";
print $fConfig "\n";
print $fConfig '"logos": ['."\n";
print $fConfig '{"link": "http://siteInstitution.fr", "img": "logoInstitution.png", "title": "nomInstitution"}'."\n";
print $fConfig "],\n";
print $fConfig "\n";
print $fConfig '"monolingual": 1,'."\n";
print $fConfig "\n";
print $fConfig '"availableLangs": {'."\n";
print $fConfig '"fr": "Français"'."\n";
print $fConfig '},'."\n";
print $fConfig "\n";
print $fConfig '"parts": {'."\n";
print $fConfig '},'."\n";
print $fConfig "\n";

print $fConfig '"criteria": {'."\n";
my $i=0;
foreach my $k (keys %corpusProperties) {
  print $fConfig '"'.$k.'": {'."\n";
  print $fConfig '"NULL'.$i.'": [';
  foreach my $v (split("\t", $corpusProperties{$k})) {
    print $fConfig "\"$v\", ";
  }
  print $fConfig '],'."\n";
  print $fConfig '},'."\n";
  ++$i;
}
print $fConfig '},'."\n";

print $fConfig '"textProperties": ["'.join('","', keys %corpusProperties).'"],'."\n";

print $fConfig '"textNotesWithProperties": true,'."\n";
print $fConfig '"textNotes": ["title", "author", "nbTokens", "sharingPolicy", "'.join('","', keys %corpusProperties).'"],'."\n";
  
print $fConfig '"contextRange": 200,'."\n";

print $fConfig '"categs": {'."\n";
foreach my $cat (keys %cats) {
  print $fConfig "\"$cat\": [\"$cat\"],\n";
}
print $fConfig '},'."\n";
  
close($fConfig);
