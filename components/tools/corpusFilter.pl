#!/usr/bin/perl -w

use strict;
use warnings;

use Getopt::Long;

# Tout passer en UTF-8
use v5.12;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Désactiver les warnings sur les fonctions "expérimentales" (qui marchent très bien).
no if ($] >= 5.018), 'warnings' => 'experimental';


############################
# Traitement des arguments #
############################

# Lecture des arguments
my $config_from = '';
my $config_to = '';
my $config_keep = 0;
GetOptions(
  'from|f=s'=>\$config_from,
  'to|t=s'=>\$config_to,
  'keep|k=s' =>\$config_keep
);

if(!$config_from || !$config_to) {
  die("Paramètre manquant.\n");
}


######################
# Traitement du flux #
######################
my $keep = ! $config_keep;  # Doit-on garder (1) ou supprimer (0) ce qui se trouve entre $config_from et $config_to ?
while(my $line = <STDIN>) {
  chomp $line;

  if($line=~m!$config_from!) {
    $keep = ! $keep; 
    print "$line\n";
  }
  elsif($line=~m!$config_to!) {
    $keep = ! $keep;
    print "$line\n";
  }
  elsif($keep) {
    print "$line\n";
  }


}
