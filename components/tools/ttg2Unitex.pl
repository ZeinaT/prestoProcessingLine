#!/usr/bin/perl

use strict;
use warnings;

# Tout passer en UTF-8
use v5.12;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";
use open qw(:std :utf8);

while (my $line = <STDIN>) {
  chomp($line);
	my ($form, $morph, $lemma, $sem, $shortMorph);
  if($line =~ m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {
    ($form, $morph, $lemma, $sem) = split(/\s*\t\s*/, $line);
    ($shortMorph = $morph) =~ s/^(.(\+.)?).*/$1/;
  }
  elsif($line =~ m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {
    ($form, $morph, $lemma) = split(/\s*\t\s*/, $line);
    ($shortMorph = $morph) =~ s/^(.(\+.)?).*/$1/;
  }
  elsif($line =~ m/^([^\t]+?)\t([^\t]+?)$/) {
    ($form, $morph) = split(/\s*\t\s*/, $line);
    ($shortMorph = $morph) =~ s/^(.(\+.)?).*/$1/;
  }
  elsif($line =~ m/^([^\t]+?)$/) {
    print "\n";
  }
	$form =~ s/[.,]/\\$&/g;
	$lemma =~ s/[.,]/\\$&/g;
	print "{$form,$lemma.$morph}\n";
}
