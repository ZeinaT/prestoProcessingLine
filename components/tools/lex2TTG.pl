#!/usr/bin/perl -w

# Entrée: forme\tpos\tlemme

use strict; 
use warnings;

use Getopt::Long;

# Tout passer en UTF-8
use v5.12;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Désactiver les warnings sur les fonctions "expérimentales" (qui marchent très bien).
no if ($] >= 5.018), 'warnings' => 'experimental';

my $dico;
my $listEtiq;
while (my $line = <STDIN>) {
  chomp($line);
  
  if($line && $line!~m/^\s*#/) {
    my ($form, $morph, $lemma) = split(/\s*\t\s*/, $line);
    if($form && $lemma && $morph) {
      push(@{ $dico->{$form} }, "$lemma\t$morph");
    } else {
      #die("Ligne mal formée: $line\n");
    }
  }
}
	       

# Écriture du lexique
{
  foreach my $form (sort keys %{$dico}) {
    print "$form";

    my $morph2Lemma;

    foreach my $lm (@{$dico->{$form}}) {
      my($lemma, $morph) = split(/\s*\t\s*/, $lm);
      $lemma=~s/\s/_/g;  # Ttg n'aime pas les espaces dans les lemmes
      push(@{$morph2Lemma->{$morph}}, $lemma);  # Regrouper les lemmes par morph
      
    }

    foreach my $morph (sort keys %{$morph2Lemma}) {
      print "\t$morph ";
      print (join('|', @{$morph2Lemma->{$morph}}));
    }
    
    print "\n";
  }
}
