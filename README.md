# Chaîne de traitement Presto

## Description

La chaîne de traitement Presto est un étiqueteur morpho-syntaxique utilisé dans le cadre du projet [Presto](http://presto.ens-lyon.fr/).

Elle utilise TreeTagger pour l'étiquetage proprement dit mais accepte en entrée des fichiers de différents formats qu'elle normalise (voir le projet [prestoNormaliser](https://gitlab.com/ANR-DFG-presto/prestoNormaliser) pour implémenter le support de nouveaux formats) avant de permettre de leur appliquer de règles de réécritures dans un DSL documenté (voir le projet [prestoLexicalRules](https://gitlab.com/ANR-DFG-presto/prestoLexicalRules)) et de les convertir au format attendu par TreeTagger.

La sortie de TreeTagger est elle-même traitée pour proposer les fichiers annotés également dans un format importable par [TXM](http://textometrie.ens-lyon.fr/) ainsi que sous forme de pages HTML.

## Dépendances
Conçu et testé sur Ubuntu 16.04. L'installeur requiert la présence de `apt`, `cpan` et `sudo` et devrait fonctionner sur toute distribution Linux à base Debian. Adaptez le nom de la commande d'installation de paquets et possiblement leurs noms ailleurs.

## Installation
./install.sh

## Utilisation

`./presto.sh` lancé seul vous affichera la liste des options disponibles. Les deux seules options obligatoires sont le chemin du corpus d'entrée et celui d'un dossier de configuration appelé «profil». Deux répertoires vides sont générés à l'installation pour vous permettre de stocker commodément vos corpus et profils, mais vous pouvez utiliser n'importe quelle ressource locale indépendament du dossier dans lequel elle se trouve.

### Corpus

Un corpus est une archive au format zip contenant les textes du corpus sans conditions sur leur emplacement à l'intérieur de l'archive et deux fichiers CSV de métadonnées :

<dl>
	<dt>files.csv<dt>
	<dd>Ce fichier doit contenir trois colonnes: <code>id</code>, <code>path</code> et <code>file</code> et chaque ligne renseigne donc dans l'ordre un identifiant unique qui permettra de faire lien avec les métadonnées associées au texte, le chemin du dossier à l'intérieur de l'archive où se trouve le fichier et enfin le nom du fichier lui-même.</dd>
	<dt>meta.csv<dt>
	<dd>Ce deuxième fichier plus libre regroupe les métadonnées associées au texte. Seule la première colonne, <code>id</code> est requise. Les autres colonnes peuvent avoir les intitulés de votre choix et constituent les méta-données qui seront associées aux textes et visibles dans TXM une fois le corpus importé. À toute ligne dans `files.csv` doit correspondre dans <code>meta.csv</code> une ligne ayant la même valeur pour la colonne <code>id</code> (comme une clef étrangère dans la terminologie des bases de données relationnelles) sans quoi le texte correspondant n'aura pas de métadonnées dans TXM.</dd>
</dl>

L'archive du corpus [noyau](http://icar.cnrs.fr/projets/presto/Presto_Noyau.zip) du projet presto constitue un exemple de référence du format décrit ci-dessus.

### Profil

Le profil est un dossier qui doit posséder une certaine structure. Nous distribuons [celui](https://gitlab.com/ANR-DFG-presto/prestoProfile) utilisé pour le projet et le dépôt correspondant contient aussi la description de la structure attendue si vous voulez créer le vôtre.

## Crédits
Développement: Achille Falaise, Alice Brenon

Financement:
* projet Presto, ANR - DFG (référence [ANR-12-FRAL-0010](http://www.agence-nationale-recherche.fr/Projet-ANR-12-FRAL-0010)), laboratoire [ICAR](http://icar.cnrs.fr)
* labex [ASLAN](https://aslan.universite-lyon.fr/presentation-labex/) (co-tutelles [ENS Lyon](http://www.ens-lyon.fr/), [CNRS](http://www.cnrs.fr/), [Université Lyon 2](https://www.univ-lyon2.fr/))

## Licence
Licence GPL v3
