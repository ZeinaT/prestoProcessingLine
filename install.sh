#!/usr/bin/env bash

# Script d'installation pour Ubuntu (fonctionne aussi sur Denécessite sudo).
# Usage: bash install.sh
# Lancer le script en tant qu'utilisateur normal, inutile d'être root. Le mot de passe sudo sera demandé pendant l'exécution.

TREETAGGER_VERSION=3.2.1
TREETAGGER_URL="http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger"
TREETAGGER_ARCHIVE="tree-tagger-linux-${TREETAGGER_VERSION}.tar.gz"
TREETAGGER="./lib/treetagger/${TREETAGGER_VERSION}"

TALISMANE_VERSION=5.1.1
TALISMANE_URL="https://github.com/joliciel-informatique/talismane/releases/download/v${TALISMANE_VERSION}"
TALISMANE_ARCHIVE="talismane-distribution-${TALISMANE_VERSION}-bin.zip"
TALISMANE="./lib/talismane/${TALISMANE_VERSION}"

download="wget -q --show-progress -c -P /tmp"

# Aller dans le dossier où se trouve le script d'installation
BASE_DIR=$(dirname "$0")
cd "$BASE_DIR"
GIT_BASE_URL=$(git config --get remote.origin.url)

# Ajouter les composants Presto (dépôts à part)
mkdir -p components
(
  cd components
  for compo in prestoLexicalRules prestoNormaliser prestoTokeniser; do
    if [ -d $compo ]; then
      git --git-dir=$compo/.git pull
    else
      git clone "${GIT_BASE_URL%/*}/$compo.git"
    fi
  done
)

printf "We will now install packaged dependencies. You may be asked to enter your password for sudo privileges"
# Installer les dépendances (testé sur Ubuntu 16.04)
sudo apt install python3-csvkit rsync
sudo cpan -i File::Basename File::Slurp Getopt::Long HTML::Escape HTML::TokeParser::Simple JSON::XS Roman Switch Text::CSV_XS Try::Tiny XML::Entities

# Dossiers pour les corpus et les profils
mkdir -p corpora
mkdir -p profiles

######################
# Ajouter les outils #
######################

# TreeTagger
if [ ! -f "${TREETAGGER}/.installed" ]
then
  mkdir -p "${TREETAGGER}"
  if [ ! -f "/tmp/${TREETAGGER_ARCHIVE}" ] || ! tar tf "/tmp/${TREETAGGER_ARCHIVE}" 2> /dev/null; then
    $download "${TREETAGGER_URL}/Tagger-Licence"
    while [ -z "${ACCEPT}" ]; do
      less "/tmp/Tagger-Licence"
      printf "Accept licence ? [y/N/s(how again)] "
      read ACCEPT
      case "${ACCEPT}" in
        y*|Y*) break;;
        s*) ACCEPT='';;
        *) exit 0;;
      esac
    done
    $download "${TREETAGGER_URL}/data/${TREETAGGER_ARCHIVE}"
  fi
  tar -xvzf "/tmp/${TREETAGGER_ARCHIVE}" -C /tmp &&
  rsync -a /tmp/bin/train-tree-tagger "${TREETAGGER}" &&
  rsync -a /tmp/bin/tree-tagger "${TREETAGGER}" &&
  touch "${TREETAGGER}/.installed"
fi

# Talismane
if [ ! -f "${TALISMANE}/.installed" ]
then
  mkdir -p "${TALISMANE}"
  if [ ! -f "/tmp/${TALISMANE_ARCHIVE}" ] || ! zip -T "/tmp/${TALISMANE_ARCHIVE}" > /dev/null; then
    $download "${TALISMANE_URL}/${TALISMANE_ARCHIVE}"
  fi
  unzip "/tmp/${TALISMANE_ARCHIVE}" -d /tmp/talismane &&
  rsync -a /tmp/talismane/* "${TALISMANE}" &&
  rm -r /tmp/talismane &&
  touch "${TALISMANE}/.installed"
fi
