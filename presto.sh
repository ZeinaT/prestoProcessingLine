#!/usr/bin/env bash

USAGE="\n
Chaîne de traitement Presto. \n
Paramètres obligatoires: \n
\t  -c Path \t      Chemin du corpus à utiliser. \n
\t  -p Path \t      Chemin du profil à utiliser. \n
Paramètres optionnels: \n
\t  -d \t\t         Mode debug. \n
\t  -v \t\t         Mode verbeux. \n
\t  -t Path \t      Dossier temporaire. \n
\t  -R \t\t         Générer un rapport à la fin. \n
\n
--- Liste des corpus: ---\n
`find ./corpora -type f -name '*.zip'`\n
\n
--- Liste des profils: ---\n
`ls ./profiles`\n
"

# Exemples d'utilisation
# Compter les balises
#   bash presto.sh -c corpora/Presto-Noyau-2017-11-06 -p profiles/presto-main | grep '<' | perl -pe 's/ .*//' | sort | uniq -c


######################
# Valeurs par défaut #
######################

CPLTARGS=''  # Arguments complémentaires à passer à tous les composants
PROFILE=''  # Profil à utiliser
TMP=/tmp/presto  # Dossier temporaire
RAPPORT=false  # Générer un rapport à la fin
CLEAN=false  # Nettoyer les ressources temporaires déjà générées (pour repartir de zéro)
TREETAGGER_VERSION=3.2.1

########################
# Paramètres du script #
########################

while getopts ":c:p:t:dvCR" opt; do
  case $opt in
    c)
      CORPUS="${OPTARG%/}"
      ;;
    p)
      PROFILE="${OPTARG%/}"
      ;;
    d)
      DEBUG=1
      ;;
    v)
      VERBOSE=1
      ;;
    t)
      TMP="${OPTARG%}"
      ;;
    C)
      CLEAN=true
      ;;
    R)
      RAPPORT=true
      ;;
    \?)
      echo "Option invalide: -$OPTARG" >&2
      echo -e $USAGE >&2
      exit 1
      ;;
    :)
      echo "L'option -$OPTARG requiert un argument." >&2
      echo -e $USAGE >&2
      exit 1
      ;;
  esac
done


#############
# Fonctions #
#############

function evalModel {
  echo ""
  echo "Évaluation du modèle"

  cat "$TMP/trainCorpus/trainCorpus.trainPart.csv" | csvcut -t -c form | csvformat -T | tail -n +2 | perl -pe 's/(.*)/lc($1)/e' > "$TMP/treetagger/trainCorpus-t.trainPart.csv"
  cat "$TMP/trainCorpus/trainCorpus.devPart.csv" | csvcut -t -c form | csvformat -T | tail -n +2 | perl -pe 's/(.*)/lc($1)/e' > "$TMP/treetagger/trainCorpus-t.devPart.csv"
  cat "$TMP/trainCorpus/trainCorpus.goldPart.csv" | csvcut -t -c form | csvformat -T | tail -n +2 | perl -pe 's/(.*)/lc($1)/e' > "$TMP/treetagger/trainCorpus-t.goldPart.csv"
  cat "$TMP/trainCorpus/trainCorpus.trainPart.csv" | csvcut -t -c form,pos | csvformat -T | tail -n +2 | perl -pe 's/(.*)\t/lc($1)."\t"/e' > "$TMP/treetagger/trainCorpus-tp.trainPart.csv"
  cat "$TMP/trainCorpus/trainCorpus.devPart.csv" | csvcut -t -c form,pos |  csvformat -T | tail -n +2 | perl -pe 's/(.*?)\t/lc($1)."\t"/e' > "$TMP/treetagger/trainCorpus-tp.devPart.csv"
  cat "$TMP/trainCorpus/trainCorpus.goldPart.csv" | csvcut -t -c form,pos | csvformat -T | tail -n +2 | perl -pe 's/(.*?)\t/lc($1)."\t"/e' > "$TMP/treetagger/trainCorpus-tp.goldPart.csv"

  lib/treetagger/$TREETAGGER_VERSION/tree-tagger -cap-heuristics -token -sgml -quiet "$TMP/treetagger/model.par" "$TMP/treetagger/trainCorpus-t.trainPart.csv" > "$TMP/treetagger/presto-tt-eval.tmp"
 diff -y "$TMP/treetagger/presto-tt-eval.tmp" "$TMP/treetagger/trainCorpus-tp.trainPart.csv" > "$TMP/treetagger/train.diff"
 NBTOTAL=$(cat "$TMP/treetagger/presto-tt-eval.tmp" | wc -l)
  NBERR=$(diff -y --suppress-common-lines "$TMP/treetagger/presto-tt-eval.tmp" "$TMP/treetagger/trainCorpus-tp.trainPart.csv" | wc -l)
  TRAIN=`echo "scale=4; ($NBTOTAL-$NBERR)/$NBTOTAL" | bc`
  
  lib/treetagger/$TREETAGGER_VERSION/tree-tagger -cap-heuristics -token -sgml -quiet "$TMP/treetagger/model.par" "$TMP/treetagger/trainCorpus-t.devPart.csv" > "$TMP/treetagger/presto-tt-eval.tmp"
  diff -y "$TMP/treetagger/presto-tt-eval.tmp" "$TMP/treetagger/trainCorpus-tp.devPart.csv" > "$TMP/treetagger/train.diff"
  NBTOTAL=$(cat "$TMP/treetagger/presto-tt-eval.tmp" | wc -l)
  NBERR=$(diff -y --suppress-common-lines "$TMP/treetagger/presto-tt-eval.tmp" "$TMP/treetagger/trainCorpus-tp.devPart.csv" | wc -l)
  DEV=`echo "scale=4; ($NBTOTAL-$NBERR)/$NBTOTAL" | bc`
  
  lib/treetagger/$TREETAGGER_VERSION/tree-tagger -cap-heuristics -token -sgml -quiet "$TMP/treetagger/model.par" "$TMP/treetagger/trainCorpus-t.goldPart.csv" > "$TMP/treetagger/presto-tt-eval.tmp"
  diff -y "$TMP/treetagger/presto-tt-eval.tmp" "$TMP/treetagger/trainCorpus-tp.goldPart.csv" > "$TMP/treetagger/train.diff"
  NBTOTAL=$(cat "$TMP/treetagger/presto-tt-eval.tmp" | wc -l)
  NBERR=$(diff -y --suppress-common-lines "$TMP/treetagger/presto-tt-eval.tmp" "$TMP/treetagger/trainCorpus-tp.goldPart.csv" | wc -l)
  GOLD=`echo "scale=4; ($NBTOTAL-$NBERR)/$NBTOTAL" | bc`
  
  echo ""
  echo "Le modèle TreeTagger est construit."
  echo "  PRÉCISION (TRAIN): $TRAIN"
  echo "  PRÉCISION (DEV): $DEV"
  echo "  PRÉCISION (GOLD): $GOLD"
}


###############
# Préparation #
###############

# Vérification du nombre d'arguments
[ -z "$CORPUS" ] && echo -e "Pas de corpus spécifié." && echo -e $USAGE >&2 && exit 1
[ -z "$PROFILE" ] && echo -e "Pas de profil spécifié." && echo -e $USAGE >&2 && exit 1
[ -z "$TMP" ] && echo "Fichier de travail non spécifié." && exit 1

# Création du dossier de travail
[ $CLEAN = true ] && rm -r "$TMP/" 2>/dev/null
mkdir -p "$TMP/trainCorpus/"
mkdir -p "$TMP/lexicon/"
mkdir -p "$TMP/texts/00_original"
mkdir -p "$TMP/texts/01_norm"
mkdir -p "$TMP/texts/02_tok"
mkdir -p "$TMP/texts/03_ttginput"
mkdir -p "$TMP/texts/04_ttgoutput"
mkdir -p "$TMP/texts/05_postAnnotationRules"
mkdir -p "$TMP/texts/meta"
mkdir -p "$TMP/treetagger"

# Peupler le dossier de travail
if [ -f "$PROFILE/resources/treetagger/"*.par ]; then  # Le profil contient des ressources précompilées pour TreeTagger
  cp "$PROFILE/resources/treetagger/"*.par "$TMP/treetagger/model.par"
fi


########################################
# Traitement du corpus d'apprentissage #
########################################

TRAINCORPUS_ARCHIVE="$PROFILE/resources/trainCorpus/trainCorpus.zip"
TRAINCORPUS_RULES="$PROFILE/resources/rules/trainCorpusRules.txt"
SENTENCE_BOUNDARY=`cat "$PROFILE/config/sentenceBoundary.txt"`

if [ ! -f "$TMP/treetagger/model.par" ] || [ ! -s "$TMP/treetagger/model.par" ]; then  # Si le résultat n'existe pas déjà

  # Préparation du corpus

  unzip -p "$TRAINCORPUS_ARCHIVE" |  # Extraire les colonnes form, lemma, POS et complément du lexique
    csvcut -t -c form,lemma,pos | csvformat -T |  # On garde seulement aux colonnes form, lemma et POS
    perl components/prestoLexicalRules/lexicalRules.pl -r "$TRAINCORPUS_RULES" ${VERBOSE:+'-v'} ${DEBUG:+'-d'} > "$TMP/trainCorpus/01_rules.csv" # Appliquer les règles

  # Division en 3

  cat "$TMP/trainCorpus/01_rules.csv" |
    perl components/tools/corpusSplitter.pl -b "pos=\"$SENTENCE_BOUNDARY\"" -p 10 -k 0,1,2,3,4,5,6,7 > "$TMP/trainCorpus/trainCorpus.trainPart.csv" &

  cat "$TMP/trainCorpus/01_rules.csv" |
    perl components/tools/corpusSplitter.pl -b "pos=\"$SENTENCE_BOUNDARY\"" -p 10 -k 8 > "$TMP/trainCorpus/trainCorpus.devPart.csv" &

  cat "$TMP/trainCorpus/01_rules.csv" |
    perl components/tools/corpusSplitter.pl -b "pos=\"$SENTENCE_BOUNDARY\"" -p 10 -k 9 > "$TMP/trainCorpus/trainCorpus.goldPart.csv" &

  wait  # Attendre que les 3 divisions soient terminées

fi


#########################
# Traitement du lexique #
#########################

LEXICON_ARCHIVE="$PROFILE/resources/lexicon/Lexicon.zip"
LEXICON_RULES="$PROFILE/resources/rules/lexiconRules.txt"

if [ ! -f "$TMP/lexicon/01_rules.csv" ] || [ ! -s "$TMP/lexicon/01_rules.csv" ] ; then  # Si le résultat n'existe pas déjà

  unzip -p "$LEXICON_ARCHIVE" > "$TMP/lexicon/00_lexicon.tmp"  # Copier le lexique dans un fichier intermédiaire pour que pv puisse afficher une barre de progression
    
  pv "$TMP/lexicon/00_lexicon.tmp" |  # Progression
    perl components/prestoLexicalRules/lexicalRules.pl -r "$LEXICON_RULES" ${VERBOSE:+'-v'} ${DEBUG:+'-d'} |  # Appliquer les règles
    csvcut -t -c form,pos,lemma | csvformat -T |  # Mettre les colonnes dans l'ordre forme, pos, lemme
   grep -vP "^\s*#" |  # Enlever les commentaires
    grep -vP "^\s*$" |  # Enlever les lignes vides
    grep -vP "^\s*form\tpos\tlemma" |  # Enlever les headers éparpillés dans le fichier
    grep -vP "^[^\t]+\t\t" |  # Enlever les lignes où il n'y a pas de POS
    sort -u > "$TMP/lexicon/01_rules.tmp"  # Trier et enlever les doublons

  # Remettre un header au début
  echo "form	pos	lemma" > "$TMP/lexicon/01_rules.csv"
  cat "$TMP/lexicon/01_rules.tmp" >> "$TMP/lexicon/01_rules.csv"
  
fi
  
  
############################################
# Création du modèle de langage TreeTagger #
############################################

if [ ! -f "$TMP/treetagger/model.par" ] || [ ! -s "$TMP/treetagger/model.par" ]; then  # Si le résultat n'existe pas déjà
  # Formatage du corpus d'apprentissage pour TreeTagger
  cat "$TMP/trainCorpus/trainCorpus.trainPart.csv" |
    csvcut -t -c form,pos,lemma | csvformat -T |
    tail -n +2 |  # Enlever le header
    grep -vP "^\s*#" |  # Enlever les commentaires
    grep -vP "^\s*$" |  # Enlever les lignes vides
    grep -vP "^\s*form\tpos\tlemma" |  # Enlever les autres headers éparpillés dans le fichier
    grep -vP "\t\t" |  # Enlever les entrées où il manque la POS  # TODO: afficher un message d'avertissement, car si on doit enelver qqch, c'est probablement qu'il y a un problème dans le corpus
    perl -pe 's/^(.*?)\t/lc($1)."\t"/e' > "$TMP/treetagger/trainCorpus.trainPart.treetagger.csv" &  # Formes en minuscules

  # Extraction du tagset pour Treetagger
  cat "$TMP/lexicon/01_rules.csv" |
    csvcut -t -c pos |  # Garder seulement les pos
    grep -vi '\(""\|pos\)' | # ni la chaîne vide '""' ni 'pos' ne sont des étiquettes
    sort -u > "$TMP/treetagger/tagset.txt" &

  # Formatage du lexique pour Treetagger  
 cat "$TMP/lexicon/01_rules.csv" |
    tail -n +2 > "$TMP/treetagger/lexicon.tmp"  # Enlever le header
   cat "$TMP/trainCorpus/trainCorpus.trainPart.csv" |  # Ajout du lexique du corpus d'entraînnement
   csvcut -t -c form,pos,lemma | csvformat -T |  # lex2TTG.pl veut des colonnes dans l'ordre forme, pos, lemme
   grep -P '|' |  # Ne pas garder les lemmes ambigus
   perl -pe 's/^(.*?)\t/lc($1)."\t"/e' |  # Formes en minuscules 
   tail -n +2 >> "$TMP/treetagger/lexicon.tmp"  # Ajouter le vocabulaire du corpus d'apprentissage  # Enlever le header

  SRC_BLACKLIST="${PROFILE}/resources/lexicon/blacklist.txt"
  DST_BLACKLIST="${TMP}/treetagger/blacklist.txt"
  if [ -f "${SRC_BLACKLIST}" ]
  then
    if [ "${DST_BLACKLIST}" -ot "${SRC_BLACKLIST}" ]
    then
      grep -v '^\(\s*#.*\)\?$' "${SRC_BLACKLIST}" > "${DST_BLACKLIST}"
    fi
 else
    touch "${DST_BLACKLIST}"
  fi

  cat "$TMP/treetagger/lexicon.tmp" |
    grep -vf "$TMP/treetagger/blacklist.txt" |
    sort -u |  # Enlever les doublons
    perl components/tools/lex2TTG.pl |  # Convertir au format TreeTagger
    perl -pe 's/(^|\t|\s|\|)([^|]+)\|\2($|\t|\s|\|)/\1\2\3/' |  # Virer les doublons
    perl -pe 's/(^|\t|\s|\|)([^|]+)\|\2($|\t|\s|\|)/\1\2\3/' |  # Oui c'est crade
    perl -pe 's/(^|\t|\s|\|)([^|]+)\|\2($|\t|\s|\|)/\1\2\3/' |  # Mais avec un s///g ça ne marche pas
    perl -pe 's/(^|\t|\s|\|)([^|]+)\|\2($|\t|\s|\|)/\1\2\3/' |  # Et en attendant comme ça, ça marche
    perl -pe 's/(^|\t|\s|\|)([^|]+)\|\2($|\t|\s|\|)/\1\2\3/' > "$TMP/treetagger/lexicon.txt"
         
  wait  # Attendre la fin de la conversion des ressources pour TreeTagger
    
  ./lib/treetagger/$TREETAGGER_VERSION/train-tree-tagger -utf8 -st $SENTENCE_BOUNDARY "$TMP/treetagger/lexicon.txt" "$TMP/treetagger/tagset.txt" "$TMP/treetagger/trainCorpus.trainPart.treetagger.csv" "$TMP/treetagger/model.par"

 [ $VERBOSE ] && evalModel
fi


########################
# Traitement du corpus #
########################

# Initialisation des variables
POSTTOKEN_RULES="$PROFILE/resources/rules/postTokenRules.txt"
POSTANNOTATION_RULES="$PROFILE/resources/rules/postAnnotationRules.txt"

# Extraction des méta-données
unzip -p "$CORPUS" files.csv | grep  '\(K262\|^id\)'  > "$TMP/files.csv" 
unzip -p "$CORPUS" meta.csv > "$TMP/meta.csv"

# Préparation des dossiers de sortie
mkdir -p "$TMP/output/html"
mkdir -p "$TMP/output/sq"
mkdir -p "$TMP/output/ttg"
mkdir -p "$TMP/output/txm"
cp "$TMP/meta.csv" "$TMP/output/txm/metadata.csv"
rm -f "$TMP/output/txm/import.xml"
mkdir -p "$TMP/output/unitex"

# Préparation du lexique pour la tokénisation
if [ "$TMP/lexicon/02_tokeniser.txt" -ot "$TMP/lexicon/01_rules.csv" ]
then
 cat "$TMP/lexicon/01_rules.csv" |
    csvcut -t -c form | csvformat -T |  # Ne garder que les formes
    grep -P "[-' ]" > "$TMP/lexicon/02_tokeniser.txt"  # Ne garder que les tokens qui contiennent des séparateurs
fi

IFS=$'\n'
nbFiles=0
nbTexts=0
for fileInfo in $(csvformat -T "$TMP/files.csv" | tail -n +2)  # Pour chaque fichier du corpus
do

  # Récupérer l'id et le path du CSV
  id="${fileInfo%%$'\t'*}"  # Id du texte
  fileInfo="${fileInfo#*$'\t'}"
  dir="${fileInfo%%$'\t'*}" # Dossier du fichier
  file="${fileInfo##*$'\t'}" # Nom du fichier

  # Cellule vide dans le CSV: réutiliser la valeur précédente
  [ -z "$id" ] && id="$oldid"
  [ -z "$dir" ] && dir="$olddir"
  path="${dir%/}/${file#/}"  # Éviter les doubles /

  # Màj des compteurs
  ((nbFiles++))
  [ "$id" != "$oldid" ] && ((nbTexts++))
  [ $VERBOSE ] && echo "[info] Traitement du texte $id (n° $nbTexts), fichier $path (n° $nbFiles)" >&2

  # Màj des valeurs précédentes
  oldid="$id"
  olddir="$dir"

  # Traitement du texte
  unzip -p "$CORPUS" "$path" |  # Lire l'archive du corpus
   tee "$TMP/texts/00_original/$id" |
    perl components/prestoNormaliser/normaliser.pl --filename "$path" --id "$id" ${DEBUG:+'-d'} ${VERBOSE:+'-v'} |  # Normalisation
    tee "$TMP/texts/01_norm/$id.xml" |  # Garder une trace du texte normalisé
    perl components/prestoTokeniser/tokenise.pl -l "$TMP/lexicon/02_tokeniser.txt" -w 5 |  # Tokenisation  # TODO calculer la taille de la fenêtre à partir du lexique
    tee "$TMP/texts/02_tok/$id.xml" |  # Garder une trace du texte tokenisé
    perl components/prestoLexicalRules/lexicalRules.pl -x -r "$POSTTOKEN_RULES" ${VERBOSE:+'-v'} ${DEBUG:+'-d'} -h "form,pos,lemma" |  # Appliquer les règles (mode XML)
    grep -vP '^\s*$' |  # Enlever les lignes vides et les lignes ne comportant que des espaces
    tail -n +2 |   # Enlever le header
    perl -pe 's/([^<])<.*?>/$1/g'  |  # Enlever les balises à l'intérieur des mots (par exemple hyphens)
    perl components/tools/corpusFilter.pl -f '^\s*<presto:text' -t '^\s*<presto:body' |  # Enlever tout ce qui est entre les balises text et body
    perl components/tools/corpusFilter.pl -f '^\s*</presto:body>' -t '^\s*</presto:text>' |  # Enlever tout ce qui est entre les balises /body et /text
    perl components/tools/corpusFilter.pl -f '^\s*<presto:out' -t '^\s*</presto:out>' |  # Enlever les parties presto:out
    perl -MXML::Entities -CS -pe 'XML::Entities::decode(all, $_)' |
    tee "$TMP/texts/03_ttginput/$id.txt" |
    lib/treetagger/$TREETAGGER_VERSION/tree-tagger -cap-heuristics -token -lemma -sgml -no-unknown -pt-with-lemma -quiet "$TMP/treetagger/model.par" |  # Analyser le texte avec TreeTagger
    tee "$TMP/texts/04_ttgoutput/$id.txt" |
    perl components/prestoLexicalRules/lexicalRules.pl -x -r "$POSTANNOTATION_RULES" ${VERBOSE:+'-v'} ${DEBUG:+'-d'} -h "form,pos,lemma" |  # Appliquer les règles post-annotation
    perl -pe 's/^(")\t(")\t([A-Za-z]+)$/$1\t$3\t$2/' |  # Pour une raison qui m'échappe, Treetagger inverse la POS et le lemme pour le caractère guillemet
    grep -v '^<.*>$' |  # À partir de ce point nous n'avons plus besoin des balises, les oublier
    tee "$TMP/texts/05_postAnnotationRules/$id.csv" > "$TMP/output/ttg/$id.csv"

  # Exporter le texte en HTML
  cat "$TMP/output/ttg/$id.csv" |
    tail -n +2 |  # Enlever le header
    perl components/tools/ttg2HTML.pl -m <(grep "^\(id\|$id\)\>" "$TMP/meta.csv") \
    > "$TMP/output/html/$id.html" &

  # Exporter le texte pour TXM
  cat "$TMP/output/ttg/$id.csv" |
   tail -n +2 |  # Enlever le header
   perl components/tools/ttg2TXM.pl > "$TMP/output/txm/$id.xml" &

  # Exporter le texte pour Unitex
  cat "$TMP/output/ttg/$id.csv" |
    tail -n +2 | # Enlever le header
    perl components/tools/ttg2Unitex.pl > "$TMP/output/unitex/$id.txt" &

  # Exporter le texte pour ScienQuest
  cat "$TMP/output/ttg/$id.csv" |
    tail -n +2 |  # Enlever le header
    perl components/tools/ttg2SQ.pl "${CORPUS##*/}" "$TMP/output/sq/" "$SENTENCE_BOUNDARY" &

  # Extraire les méta-données pour le rapport final
  cat "$TMP/texts/01_norm/$id.xml" | grep '<presto:text' | perl -pe 's/(<presto:text |>)//g' | perl -pe 's/[a-zA-Z]+=/\t/g' > "$TMP/texts/meta/$id.tsv"

  wait  # Attendre la fin des exportations

done


###########
# Rapport #
###########

if [ $RAPPORT = true ]
then
  echo "" >&2
  echo "Rapport dans $TMP/rapport.csv" >&2
  echo "  Textes:" >"$TMP/rapport.csv"
  echo "    $nbTexts textes source traités." >> "$TMP/rapport.csv"
  echo "    $nbFiles fichiers source traités." >> "$TMP/rapport.csv"
  cat $TMP/texts/meta/*.tsv >> "$TMP/rapport.tsv"
fi
